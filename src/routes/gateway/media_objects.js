import clientPromise from './_mongodb-client'
import dotenv from 'dotenv';
dotenv.config();

const mode = process.env['MODE'];
const _db = mode == 'development' ? 'mydb' : 'stable'

export async function post(req, res, next) {
  var data = req.body
  console.log("---server data---", data)

  const { moneys } = req.body;
  console.log("---server moneys---", moneys)

//TODO: move db operations to separate middleware
  const dbConnection = await clientPromise;
  const db = dbConnection.db(_db);
  const collection = db.collection('transactions');

  const item = {
    ...req.body
  }

  const dbResponse = await collection.insertOne(item);
  const { insertedId: _id } = dbResponse;
  const _item = await collection.findOne({ _id });


  console.log("---server _item---", _item)


  res.setHeader('Content-Type', 'application/json');

  const message = {
    status: 200,
    success: true,
    result: _item
  }

  const response = {
    method: 'POST',
    body: message,
    headers: {
      'Content-Type': 'application/json'
    }
  };

  console.log("---server response---", response)

  return res.end(JSON.stringify(response));
}


export async function get(req, res, next) {

  const dbConnection = await clientPromise;
  const db = await dbConnection.db(_db);
  const collection = await db.collection('transactions');
  const _items = await collection.find({}).toArray();

  console.log("---server _items---", _items)

  res.setHeader('Content-Type', 'application/json');

  const message = {
    status: 200,
    success: true,
    result: _items
  }

  const response = {
    method: 'GET',
    body: message,
    headers: {
      'Content-Type': 'application/json'
    }
  };

  console.log("---server response---", response)

  return res.end(JSON.stringify(response));
}