
const getYearData = () => {
  const now = new Date();
  const year = now.getFullYear();


  const monthsNames = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
  ];
  const dayNames = [
    "Воскресенье",
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
  ];


  const getMonthsFunc = () => {
    // console.log("---year---", year);

    let monthsInYearCount = 12;

    let result = {};

    for (let index = 0; index < monthsInYearCount; index++) {
      const date = new Date(year, index);

      const monthNum = date.getMonth();
      // console.log("---monthNum---", monthNum);

      const monthName = monthsNames[monthNum];
      // console.log("---monthName---", monthName);

      const monthTimestamp = date.getMilliseconds();

      const month = {
        date,
        num: monthNum,
        name: monthName,
        timestamp: monthTimestamp,
      };

      result[monthName] = month;
    }

    return result;
  };

  const getDaysFunc = () => {
    let result = {};

    for (let index = 0; index < monthsNames.length; index++) {
      const _monthName = monthsNames[index];
      const _month = monthsData[_monthName];
      // console.log("---monthsData---", monthsData);
      // console.log("---_month---", _month);

      // console.log("---_month---", _month);
      const { date, num, name, timestamp } = _month;
      // console.log("---timestamp---", timestamp);
      // console.log("---date---", date);

      const daysMax = 32;

      let _monthDaysData = { days: [] };

      for (let index = 0; index < daysMax; index++) {
        const date = new Date(year, num, index);
        const monthNum = date.getMonth();

        if (monthNum == num) {
          const dayNum = date.getDay();
          const dayName = dayNames[dayNum];
          const dayTimestamp = date.getTime() / 1000;

          const day = {
            date,
            num: dayNum,
            name: dayName,
            timestamp: dayTimestamp,
          }


          _monthDaysData.days.push(day);
          // _monthDaysData.days.push(date);
        }
      }

      result[name] = _monthDaysData;
    }

    // console.log("---result--", result);

    return result;
  };

  const monthsData = getMonthsFunc();

  const daysData = getDaysFunc();

  // console.log("---monthsData----", monthsData);
  // console.log("---daysData----", daysData);

  let result = []

  for (let index = 0; index < monthsNames.length; index++) {
    const monthName = monthsNames[index];

    result.push({
      ...monthsData[monthName],
      ...daysData[monthName]
    })

  }

  console.log("---result----", result);

  return result;

};

export default getYearData;