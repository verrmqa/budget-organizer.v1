import axios from 'axios';

const HttpServiceError = function (result, message, fileName, lineNumber) {
  const _error = new Error(message, fileName, lineNumber);

  _error.name = 'HttpService Error';
  _error.result = result;

  Object.setPrototypeOf(_error, Object.getPrototypeOf(this));
  Error.captureStackTrace(_error, HttpServiceError);

  return _error;
}

HttpServiceError.prototype = Object.create(Error.prototype, {
  constructor: {
    value: Error,
    enumerable: false,
    writable: true,
    configurable: true
  }
});

Object.setPrototypeOf(HttpServiceError, Error);

const HttpService = {
  http: axios.create(),
  success: (data) => ({ 
    ...data,
    result: data
  }),
  error: (e) => {
    console.log(e);

    const { response, request } = e;
    const host = request._options ? request._options.hostname : request.host;

    let notification = `Notification`;
    let message = 'Unauthorized';
    let level = 'warning';

    if (!response || response.status !== 401) {
      notification = `Service ${host} is unavailable.`;
      message = `Something went wrong. Already working on it!.`;
      level = 'error';
    }

    throw new HttpServiceError({
      status: response && response.status ? response.status : 500,
      success: response && response.success ? response.success : false,
      event: response && response.event ? response.event : 'SERVER_ERROR',
      result: {
        message
      }
    }, notification);
  }
}

module.exports = HttpService;